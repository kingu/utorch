# uTorch

Simple and elegant torch application for Ubuntu Phone.

Most devices do have a flashlight switch in the indicator. They will not need this app. It may not even work.
But some devices don't have this switch. They will love this app.

Earlier versions where reported to work with the following devices:
- E4.5, E5
- Meizu MX4, Meizu Pro5
- Nexus 4
-
Tested with focal on:
- OP5T
- Fairphone 4
- Vollaphone
- Xiaomi MI A2

*For the torch to work, the app needs access to camera. Please allow access when prompted.*

This fork of the project is to keep the app maintained and available for Ubuntu Touch.

## Translating

uTorch app can be translated on [Hosted Weblate](https://hosted.weblate.org/projects/ubports/utorch/). The localization platform of this project is sponsored by Hosted Weblate via their free hosting plan for Libre and Open Source Projects.

We welcome translators from all different languages. Thank you for your contribution!
You can easily contribute to the localization of this project (i.e. the translation into your language) by visiting (and signing up with) the Hosted Weblate service as linked above and start translating by using the webinterface. To add a new language, log into weblate, goto tools --> start new translation.

## Credits

The initial code of this app is taken from https://launchpad.net/utorch assigned to Szymon Waliczek. Until now the app has been coded by others.
Given the credits in that versions about page, I assume that not only Szymon but also others did some coding or contribution:

Szymon Waliczek         email: majsterrr@gmail.com          role: Author and main developer until 2017

Michael Zanetti         email: michael_zanetti@gmx.net      role: Developer

Niklas Wenzel           email: nikwen.developer@gmail.com   role: Developer

Nekhelesh Ramananthan   email: nik90@ubuntu.com             role: Developer

Sam Hewitt              email: snwh@ubuntu.com              role: Designer


Many thanks to all of you for your work in creating this cool app!
