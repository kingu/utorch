import QtQuick 2.12
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

Item {
    id: root
    width: units.gu(15)
    height: width

    RectangularGlow {
        id: effect
        anchors.fill: button
        glowRadius: button.state == "on" ? units.gu(0.5) : -units.gu(5)
        spread: 0.2
        color: "#fffb91"
        cornerRadius: button.radius + glowRadius
        Behavior on glowRadius {
            NumberAnimation {}
        }
    }


    Rectangle {
        id: button

        width: parent.width - units.gu(8)
        height: parent.height - units.gu(8)
        anchors.centerIn: parent
        color: LomiriColors.red
        radius: width*0.5
        clip: true

        state: flashOn ? "on" : "off"

        states: [
            State {
                name: "on"
            },
            State {
                name: "off"
            }
        ]

        transitions: Transition {
            NumberAnimation { properties: "radius"; easing.type: Easing.InOutQuad; duration: 300 }
            ColorAnimation { properties: "color" ; }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: flashOn = !flashOn
    }

    Flipable {
        id: flipable
        width: units.gu(8)
        height: width
        anchors.centerIn: parent
        front: Icon { name: "torch-off"; anchors.centerIn: parent; color: "white"; width: units.gu(8) }
        back: Icon { name: "torch-on"; anchors.centerIn: parent; color: "white"; width: units.gu(8) }

        transform: Rotation {
            id: rotation
            origin.x: flipable.width/2
            origin.y: flipable.height/2
            axis.x: 1; axis.y: 0; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 0    // the default angle
        }

        states: State {
            name: "back"
            PropertyChanges { target: rotation; angle: 180 }
            when: button.state === "on"
        }
        transitions: Transition {
            NumberAnimation { target: rotation; property: "angle"; easing.type: Easing.InOutQuad; duration: 400 }
        }
    }
}
