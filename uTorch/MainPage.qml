import QtQuick 2.12
import Lomiri.Components 1.3
import QtMultimedia 5.0
import QtQuick.Window 2.2

Page {
    id: root
    visible: false


    // to determin in camera failed to connect
    Connections {
        target: Qt.application
        onActiveChanged: if(Qt.application.active && camera.failedToConnect) rebootTimer.start()
    }

    Component.onCompleted: rebootTimer.start()

    Timer {
        id: rebootTimer
        running: false
        interval: 400
        onTriggered: camera.rebootCamera()
    }

    header: PageHeader {
        id: header
        title: "uTorch"
        StyleHints {
            backgroundColor: LomiriColors.coolGrey
        }
        trailingActionBar.actions: [
            Action {
                iconName: "settings"
                text: i18n.tr("More")
                        onTriggered: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    // slider theme component
    Component {
        id: themeSliderComponent
        TorchSwitch {
            id: torchSwitch
            anchors.centerIn: parent
            enabled: !camera.failedToConnect
            opacity: camera.failedToConnect ? 0.6 : 1
            Behavior on opacity { LomiriNumberAnimation {}  }
        }
    }

    // circle theme component
    Component {
        id: themeCircleComponent
        TorchButton {
            width: units.gu(20)
            height: units.gu(20)
            enabled: !camera.failedToConnect
            opacity: camera.failedToConnect ? 0.6 : 1
            anchors.centerIn: parent
            Behavior on opacity { LomiriNumberAnimation {}  }
        }
    }

    // loader to load different theme.
    Loader {
        anchors.centerIn: parent
        sourceComponent: settings.themeName === "Circle" ? themeCircleComponent : themeSliderComponent
    }

    // Warning that uTorch doesnt have access to camera.
    ListItem {
        id: noCameraAccessHint
        anchors.top: header.bottom
        height: 0
        color: LomiriColors.blue

        // animation of hint/notification SHOW
        SequentialAnimation {
            running: camera.failedToConnect
            PauseAnimation { duration: 1500 }
            LomiriNumberAnimation { target: noCameraAccessHint; property: "height"; to: noCameraAccessHintLayout.height + units.gu(1) ; duration: 100; }
        }

        // animation of hint/notification HIDE
        SequentialAnimation {
            running: !camera.failedToConnect
            LomiriNumberAnimation { target: noCameraAccessHint; property: "height"; to: 0 ; duration: 70; }
        }

        ListItemLayout {
            id: noCameraAccessHintLayout
            title.text: "uTorch does not have permission to access the camera hardware or another error occurred."
            title.wrapMode: Text.Wrap
            title.maximumLineCount: 4
            title.horizontalAlignment: Text.AlignHCenter
            subtitle.text: "If granting permission does not resolve the problem, reboot your device."
            subtitle.wrapMode: Text.Wrap
            subtitle.maximumLineCount: 4
            subtitle.horizontalAlignment: Text.AlignHCenter
            subtitle.color: "white"
            summary.text: i18n.tr("Tap on me to go to settings")
            summary.wrapMode: Text.Wrap
            summary.maximumLineCount: 3
            summary.horizontalAlignment: Text.AlignHCenter
            summary.color: LomiriColors.coolGrey
            Icon {
                name: "dialog-warning-symbolic"
                SlotsLayout.position: SlotsLayout.Leading;
                width: units.gu(4)
                anchors.verticalCenter: parent.verticalCenter
                color: "white"
            }
            Icon {
                name: "dialog-warning-symbolic"
                SlotsLayout.position: SlotsLayout.Trailing;
                width: units.gu(4)
                anchors.verticalCenter: parent.verticalCenter
                color: "white"
            }
        }
        onClicked: Qt.openUrlExternally("settings:///system/security-privacy?service=camera");
    }

    // Bottom edge
    BottomEdge {
        id: bottomEdge
        height: parent.height
        width: parent.width
        hint.text: i18n.tr("Screen Torch")
        hint.status: BottomEdgeHint.Locked
        hint.iconName: "phone-smartphone-symbolic"


        contentComponent: Rectangle {
            width: bottomEdge.width
            height: bottomEdge.height
            color: "black"

            Rectangle {
                id: bottomEdgeShader
                anchors.fill: parent
                opacity: 1

            }

            Button {
                text: i18n.tr("Close")
                width: parent.width - units.gu(6)
                color: LomiriColors.red
                anchors {
                    bottom: parent.bottom
                    bottomMargin: units.gu(2)
                    horizontalCenter: parent.horizontalCenter
                }

                onClicked: bottomEdge.collapse()
            }

            Slider {
                function formatValue(v) { return "" }
                width: parent.height - units.gu(15)
                rotation: 270
                anchors {
//                                bottom: parent.bottom
                    centerIn: parent

                    bottomMargin: units.gu(2)
                    horizontalCenter: parent.horizontalCenter
                }

                onValueChanged: bottomEdgeShader.opacity = value

                minimumValue: 0
                maximumValue: 1
                stepSize: 0.1
                value: 1
                live: true
            }


        }

        onCommitCompleted: {
            bottomEdge.hint.status = BottomEdgeHint.Locked
            main_window.visibility = Window.FullScreen
            flashOn = false
        }
        onCollapseCompleted: {
            bottomEdge.hint.status = BottomEdgeHint.Locked
            main_window.visibility = Window.AutomaticVisibility
        }
    }

    // this to be implemented.
    Connections {
        id: permissionErrorMonitor
        property var currentPermissionsDialog: null
        target: camera
        onError: {
            if (errorCode == Camera.ServiceMissingError) {
                camera.failedToConnect = true
            }
        }
        onCameraStateChanged: {
            if (camera.cameraState != Camera.UnloadedState) {
                camera.failedToConnect = false
            } else {
                camera.failedToConnect = true
            }
        }
    }

    // camera to control torch
    Camera {
        id: camera
        flash.mode: flashOn ? Camera.FlashVideoLight : Camera.FlashOff
        property bool failedToConnect: false

        function rebootCamera() {
            camera.cameraState = Camera.LoadedState;
            camera.cameraState = Camera.ActiveState;
        }
    }

    // VideoOutput - workaround for Meizu Pro5
    VideoOutput {
        id: videoOutput
        source: camera
        anchors.top: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: 1
        width: height
    }
}
