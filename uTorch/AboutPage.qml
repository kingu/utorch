/*
 * Copyright (C) 2013-2016 Szymon Waliczek.
 *
 * Authors:
 *  Szymon Waliczek <majsterrr@gmail.com>
 *  Niklas Wenzel <nikwen.developer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem2

//import Lomiri.Components.Popups 1.3
//import "../components"

Page {
    id: root

    header: PageHeader {
        id: pageHeader
        title: "uTorch"
        sections.model: [i18n.tr("Settings"), i18n.tr("About"), i18n.tr("Credits"), i18n.tr("F.A.Q")]
        sections.width: root.width

        StyleHints {
            ignoreUnknownProperties: true
            backgroundColor: LomiriColors.coolGrey
            sectionsColor: "red"
            selectedSectionColor: "red"
        }
    }

    function selectTheme(theme_name) {
        switch (theme_name) {
        case "Slider":
            settings.themeName = "Slider"
            break;
        case "Circle":
            settings.themeName = "Circle"
            break;
        }
    }


    ListModel {
        id: contributorsModel
        ListElement { name: "Daniel Frost"; email: 'one@frostinfo.de'; role: "Maintainer" }
        ListElement { name: "Szymon Waliczek"; email: 'majsterrr@gmail.com'; role: "Author" }
        ListElement { name: "Michael Zanetti"; email: "michael_zanetti@gmx.net"; role: "Developer" }
        ListElement { name: "Niklas Wenzel"; email: "nikwen.developer@gmail.com"; role: "Developer" }
        ListElement { name: "Nekhelesh Ramananthan"; email: "nik90@ubuntu.com"; role: "Developer" }
        ListElement { name: "Sam Hewitt"; email: "snwh@ubuntu.com"; role: "Designer" }
        ListElement { name: "others as listed on Gitlab"; email: "https://gitlab.com/Danfro/utorch/-/graphs/main"; role: "Contributors" }

        function dummyFunction() {
            var a = i18n.tr("Author")
            var b = i18n.tr("Developer")
            var c = i18n.tr("Designer")
            var d = i18n.tr("Maintainer")
            var e = i18n.tr("Contributors")
            var f = i18n.tr("others as listed on Gitlab")
        }
    }

    function changeFlashSettings() {
        if(settings.torchAutoStart === true) {
            settings.torchAutoStart = false
        } else {
            settings.torchAutoStart = true
        }
    }

    // F.A.Q listmodel
    ListModel {
        id: faqModel

        function loadListModel() {
            faqModel.append({ "question": i18n.tr("Which devices are supported?"), "answer": i18n.tr("Please see the README at gitlab for an up to date list:"), "extLink": '<a href=\"https://gitlab.com/Danfro/utorch/-/blob/main/README.md\">README</a>' })
            faqModel.append({ "question": i18n.tr("I blocked uTorch from using Camera.Service.<br>How do I fix it?"), "answer": i18n.tr("Open System Settings <b>\></b> Security and Privacy <b>\></b> App Permissions <b>\></b> Camera<br> and then make sure uTorch is <b>ticked</b> there."), "extLink":  i18n.tr("%1 Open System Settings %2").arg('<a href=\"settings:///system/security-privacy?service=camera"\">').arg('</a>') })
            //            faqModel.append({ "question": i18n.tr(""), "answer": i18n.tr(""), "extLink": '<a href=\"settings:///system/security-privacy"\">Text to be displayed</a>' })

        }

        // workaround to get i18n.tr() working...
        Component.onCompleted: loadListModel()
    }

    VisualItemModel {
        id: tabs

        //Settings Tab
        Item {
            width: tabView.width
            height: tabView.height
            Flickable {
                anchors.fill: parent
                contentHeight: settingsColumn.height
                clip: true
                flickableDirection: Flickable.AutoFlickIfNeeded

                Column {
                    id: settingsColumn
                    width: parent.width
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }

                    //general
                    ListItem {
                        height: units.gu(3)
                        color: LomiriColors.inkstone
                        Label {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: units.gu(1)
                            font.italic: true
                            text: i18n.tr("General")
                        }
                    }

                    // autostart
                    ListItem {
                        width: parent.width
                        ListItemLayout {
                            id: layout
                            title.text: i18n.tr("Auto torch on")
                            subtitle.text: i18n.tr("The app will start with the torch turned on")

                            Switch {
                                id: controlSwith
                                SlotsLayout.position: SlotsLayout.Trailing;
                                checked: settings.torchAutoStart
                                onClicked: changeFlashSettings()
                            }

                        }
                        onClicked: changeFlashSettings()

                    }

                    ListItem {
                        height: units.gu(3)
                        color: LomiriColors.inkstone
                        Label {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: units.gu(1)
                            font.italic: true
                            text: i18n.tr("Themes")
                        }
                    }

                    // Slider theme button
                    ListItem {
                        ListItemLayout {
                            //theme preview
                            Image {
                                source: "slider.png"
                                SlotsLayout.position: SlotsLayout.Leading;
                                fillMode: Image.PreserveAspectFit
                                width: units.gu(3)
                            }
                            //text
                            title.text: i18n.tr("Slider")
                            //tick
                            Icon {
                                name: "tick"
                                width: units.gu(2.5)
                                SlotsLayout.position: SlotsLayout.Trailing
                                color: "white"
                                opacity: settings.themeName === "Slider"
                                Behavior on opacity { LomiriNumberAnimation {} }
                            }
                        }
                        onClicked: selectTheme("Slider")
                    }

                    // Circle theme button
                    ListItem {
                        ListItemLayout {
                            // theme preview
                            Image {
                                source: "circle.png"
                                SlotsLayout.position: SlotsLayout.Leading;
                                fillMode: Image.PreserveAspectFit
                                width: units.gu(3)
                                y: parent.mainSlot.y
                            }
                            //text
                            title.text: i18n.tr("Circle")
                            //tick
                            Icon {
                                name: "tick"
                                width: units.gu(2.5)
                                SlotsLayout.position: SlotsLayout.Trailing
                                color: "white"
                                opacity: settings.themeName === "Circle"
                                Behavior on opacity { LomiriNumberAnimation {} }
                            }
                        }
                        onClicked: selectTheme("Circle")
                    }
                }
            }
        }

        // About Tab
        Item {
            width: tabView.width
            height: tabView.height
            Flickable {
                anchors.fill: parent
                contentHeight: aboutTabColumn.height
                clip: true
                flickableDirection: Flickable.AutoFlickIfNeeded

                Column {
                    id: aboutTabColumn
                    spacing: units.gu(4)
                    anchors { left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter; margins: units.gu(2) }

                    Item {
                        width: parent.width
                        height: units.gu(2)
                    }

                    LomiriShape {
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: units.gu(20)
                        height: units.gu(20)
                        source: Image {
                            source: Qt.resolvedUrl("uTorch.png")
                        }
                    }

                    Column {
                        width: parent.width
                        spacing: units.gu(0.5)

                        Label {
                            width: parent.width
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: "uTorch"
                            font.bold: true
                            color: "white"
                            fontSize: "large"
                            horizontalAlignment: Text.AlignHCenter
                        }

                        Label {
                            width: parent.width
                            text: i18n.tr("Torch application for Ubuntu Touch devices")
                            horizontalAlignment: Text.AlignHCenter
                            color: "white"
                            wrapMode: Text.WordWrap
                        }
                    }

                    Column {
                        width: parent.width

                        Label {
                            text: i18n.tr("Released under the terms of GNU GPL v3 or higher")
                            width: parent.width
                            color: "white"
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            fontSize: "small"
                        }

                        Label {
                            text: "(C) 2016 Szymon Waliczek <a href=\"mailto://majsterrr@gmail.com\">majsterrr@gmail.com</a>"
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            linkColor: "#00C2FF"
                            color: "white"
                            fontSize: "small"
                            onLinkActivated: Qt.openUrlExternally(link)
                        }
                    }

                    Label {
                        text: i18n.tr("Source code on %1").arg("<a href=\"https://gitlab.com/Danfro/utorch\">Gitlab</a>")
                        width: parent.width
                        color: "white"
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        linkColor: "#00C2FF"
                        fontSize: "small"
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                    Label {
                        text: i18n.tr("Translate on %1").arg("<a href=\"https://hosted.weblate.org/projects/ubports/utorch/\">UBports Hosted Weblate</a>")
                        width: parent.width
                        color: "white"
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        linkColor: "#00C2FF"
                        fontSize: "small"
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                    Label {
                        text: i18n.tr("Donate me a coffee: %1").arg("<a href=\"https://paypal.me/payDanfro\">https://paypal.me/payDanfro</a>")
                        width: parent.width
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        linkColor: "#00C2FF"
                        color: "white"
                        fontSize: "small"
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                }
            }
        }

        // Contributors TAB
        Item {
            width: tabView.width
            height: tabView.height

            ListView {
                id: contributorsList
                clip: true


                anchors.fill: parent
                model: contributorsModel
                section.property: "role"
                section.labelPositioning: ViewSection.InlineLabels

                section.delegate: ListItem {
                    height: headerText.implicitHeight + units.gu(1)
                    //                    divider.visible: true
                    Rectangle {
                        id: labelShader
                        anchors.fill: parent
                        opacity: 0.1
                    }

                    Label {
                        id: headerText
                        z: labelShader.z + 2
                        text: i18n.tr(section) + ":"
                        color: "#dedede"
                        //                        font.bold: true
                        anchors { left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter;
                            leftMargin: units.gu(1); rightMargin: units.gu(1); topMargin: units.gu(0.5);
                            bottomMargin: units.gu(0.5)
                        }
                    }
                }

                delegate: ListItem {
                    divider.visible: false
                    height: mainColumn1.height + units.gu(1.5)
                    Column {
                        id: mainColumn1
                        anchors { left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter;
                            leftMargin: units.gu(2); rightMargin: units.gu(2);
                        }

                        Label {
                            text: i18n.tr(name)
                            color: "white"
                            width: parent.width
                            elide: Text.ElideRight
                        }

                        Label {
                            id: emailLabel
                            text: '<a href=\"mailto:' + email + '\">' + email + '</a>'
                            linkColor: "#00C2FF"
                            fontSize: "small"
                            width: parent.width
                            elide: Text.ElideRight
                            onLinkActivated: Qt.openUrlExternally(link)
                        }
                    }
                    onClicked: {
                        Qt.openUrlExternally("mailto:"+email)
                    }
                }
            }
        }


        // F.A.Q TAB
        Item {
            width: tabView.width
            height: tabView.height

            ListView {
                id: faqTab
                clip: true
                anchors.fill: parent
                model: faqModel
                delegate: ListItem {
                    height: faqDelegate.height + units.gu(1)
                    Column {
                        id: faqDelegate
                        anchors { left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter; margins: units.gu(2) }

                        Row {
                            width: parent.width
                            height: questionLabel.height
                            spacing: units.gu(0.5)
                            Label {
                                text: "Q:"
                                color: "white"
                            }

                            Label {
                                id: questionLabel
                                text: i18n.tr(question)
                                color: "white"
                                width: parent.width - units.gu(2)
                                wrapMode: Text.Wrap
                                //                                elide: Text.ElideRight
                            }
                        }

                        Row {
                            width: parent.width
                            height: answerLabel.height
                            spacing: units.gu(0.5)
                            Label {
                                color: "#00C2FF"
                                text: "A:"
                            }

                            Label {
                                id: answerLabel
                                color: "#00C2FF"
                                width: parent.width - units.gu(2)
                                wrapMode: Text.Wrap
                                text: i18n.tr(answer) + " " + i18n.tr(extLink)
                                onLinkActivated: Qt.openUrlExternally(link)
                                linkColor: LomiriColors.orange
                                //                                elide: Text.ElideRight
                            }
                        }
                    }
                }
            }
        }
    }

    ListView {
        id: tabView

        model: tabs
        interactive: false
        anchors {
            top: root.header.bottom
            left: root.left
            right: root.right
            bottom: root.bottom
        }

        orientation: Qt.Horizontal
        snapMode: ListView.SnapOneItem
        currentIndex: pageHeader.sections.selectedIndex
        highlightMoveDuration: LomiriAnimation.SlowDuration
    }
}
