import QtQuick 2.12
import Lomiri.Components 1.3
import Qt.labs.settings 1.0
import QtMultimedia 5.4
import QtQuick.Window 2.2
import QtSystemInfo 5.0
//import Lomiri.Components.Pickers 1.3

Window {
    id: main_window
    width: units.gu(50)
    height: units.gu(75)

    property int flashOn

    // autostart torch is set in settings
    Component.onCompleted: {
        if(settings.torchAutoStart) flashOn = true
        i18n.domain = "utorch.danfro" // needs to be the same as filename of .pot file
    }

    // vibration when torch switch used.
    onFlashOnChanged: {
        Haptics.play()
        print("torch is now", flashOn)
    }

    // App Settings
    Settings {
        id: settings
        property bool firstStart: true
        property bool torchAutoStart: false
        property string themeName: "Circle"
    }

    // Main Loader to load welcome screen or mainPage
    Loader {
        id: mainPageLoader
        anchors.fill: parent
        sourceComponent: settings.firstStart ? welcomeScreenComponent : mainPageComponent
        onLoaded: {
            pageStack.clear()
            pageStack.push(mainPageLoader.item)
        }
    }

    // MainPage component
    Component {
        id: mainPageComponent
        MainPage {
            id:mainPage
        }
    }

    // WelcomeScreen component
    Component {
        id: welcomeScreenComponent
        WelcomeScreen {
            id: welcomeScreen
        }
    }

    //Mainview (used to have full screen functionality)
    MainView {
        id: mainView
        // Note! applicationName needs to match the "name" field of the click manifest
        applicationName: "utorch.danfro"
        anchors.fill: parent
        backgroundColor: LomiriColors.coolGrey

        PageStack {
            id: pageStack
            anchors.fill: parent
        }
    }

    // ScreenSaver (to keep screen on then app has focus)
    ScreenSaver {
        id: screenSaver
        screenSaverEnabled: !(Qt.application.state == Qt.ApplicationActive)
    }
}
